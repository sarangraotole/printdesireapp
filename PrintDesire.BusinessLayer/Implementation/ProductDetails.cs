﻿using PrintDesire.BusinessLayer.Interface;
using PrintDesire.BusinessLayer.Models;
using PrintDesire.Repository.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintDesire.BusinessLayer.Implementation
{
    public class ProductDetails:IProductDetails
    {
        private ProductDetailsRepository objProductDetailsRepository;
        private ProductCategoryRepository objProductCategoryRepository;

        /// <summary>
        /// Initializes repository for 1. Mobile Model, 2 Mobile Company
        /// </summary>
        public ProductDetails()
        {
            objProductDetailsRepository = new ProductDetailsRepository();
            objProductCategoryRepository = new ProductCategoryRepository();
        }

        /// <summary>
        /// Gets all Mobile Model
        /// </summary>
        /// <returns>Mobile Model collection</returns>
        public IEnumerable<ProductDetailsModel> GetProductDetails()
        {
            var result = objProductDetailsRepository.GetProductDetails();
            return result.ToModelEntity().ToList();
        }

        /// <summary>
        /// Gets Mobile Model by ID
        /// </summary>
        /// <param name="mobileModelId"></param>
        /// <returns>Mobile Model</returns>
        public ProductDetailsModel GetProductDetailsById(int ProductDetailsId)
        {
            var result = objProductDetailsRepository.GetProductDetailsById(ProductDetailsId);

            return result.ToModelEntity();
        }

        /// <summary>
        /// Inserts Mobile Model
        /// </summary>
        /// <param name="objMobileModelDetails"></param>
        /// <returns>Success result true/false</returns>
        public bool AddProductDetails(ProductDetailsModel objProductDetailsModel)
        {
            var productDetails = objProductDetailsModel.ToModelEntity();

            //var mobileCompany = objMobileCompanyRepository.GetMobileCompanyById(objMobileModelDetails.MobileCompanyId);

            //mobileModel.mobilecompany = mobileCompany;

            var result = objProductDetailsRepository.AddProductDetails(productDetails);

            return result;
        }

        /// <summary>
        /// Updates Mobile Model
        /// </summary>
        /// <param name="objMobileModelDetails"></param>
        /// <returns>Success result true/false</returns>
        public bool UpdateProductDetails(ProductDetailsModel objProductDetails)
        {
            var productDetails = objProductDetails.ToModelEntity();

            var result = objProductDetailsRepository.UpdateProductDetails(productDetails);

            return result;
        }

        /// <summary>
        /// Removes Mobile Model
        /// </summary>
        /// <param name="objMobileModelDetails"></param>
        /// <returns>Success result true/false</returns>
        public bool RemoveProductDetails(ProductDetailsModel objProductDetails)
        {
            var mobileModel = objProductDetails.ToModelEntity();

            var result = objProductDetailsRepository.RemoveProductDetails(mobileModel);

            return result;
        }

        /// <summary>
        /// Gets Mobile Model By Mobile Company ID
        /// </summary>
        /// <param name="mobileCompanyId"></param>
        /// <returns>Success result true/false</returns>
        public bool GetProductDetailsByProductCategoryId(int productCategoryId)
        {
            var result = objProductDetailsRepository.GetProductDetailsByProductCategoryId(productCategoryId);

            if (result)
            {
                return true;
            }

            return false;
        }
    }
}
