﻿using PrintDesire.BusinessLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintDesire.BusinessLayer.Interface
{
   public interface IProductCategory
    {

       IEnumerable<ProductCategoryModel> GetProductCategory();


       ProductCategoryModel GetProductCategoryById(int productCategoryId);


       bool AddProductCategory(ProductCategoryModel objProductCategoryModel);


       bool UpdateProductCategory(ProductCategoryModel objProductCategoryModel);


       bool RemoveProductCategory(ProductCategoryModel objProductCategoryModel);
    }

}
