﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintDesire.BusinessLayer.Models
{
   public class ProductDetailsModel
    {
       public int ProductDetailsId { get; set; }
        
        public int ProductCategoryId { get; set; }                
        
        public string Description { get; set; }
        
        public string ImagePath { get; set; }

        public string IdealFor { get; set; }

        public string Finishing { get; set; }

        public string Color { get; set; }

        public bool? WaterProof { get; set; }

        public string Material { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public bool? IsActive { get; set; }
    }
}
