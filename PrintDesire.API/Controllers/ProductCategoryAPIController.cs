﻿using PrintDesire.BusinessLayer.Implementation;
using PrintDesire.BusinessLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PrintDesire.API.Controllers
{
    public class ProductCategoryAPIController : ApiController
    {
        ProductCategory objProductCategory = null;

        public ProductCategoryAPIController()
        {
            objProductCategory = new ProductCategory();
        }

        public IEnumerable<ProductCategoryModel> Get()
        {
            var result = objProductCategory.GetProductCategory();

            return result;
        }

        public ProductCategoryModel Get(int productCategoryId)
        {
            var result = objProductCategory.GetProductCategoryById(productCategoryId);

            return result;
        }

        public bool Post([FromBody]ProductCategoryModel objProductCategoryModel)
        {
            var result = objProductCategory.AddProductCategory(objProductCategoryModel);

            return result;
        }

        public bool Put([FromBody]ProductCategoryModel objProductCategoryModel)
        {
            var result = objProductCategory.UpdateProductCategory(objProductCategoryModel);

            return result;
        }

        public bool Delete(int productCategoryId)
        {
            var productCategoryModel = objProductCategory.GetProductCategoryById(productCategoryId);

            var result = objProductCategory.RemoveProductCategory(productCategoryModel);

            return result;
        }

    }
}
