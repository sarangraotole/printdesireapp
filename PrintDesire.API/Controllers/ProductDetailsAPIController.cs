﻿using PrintDesire.BusinessLayer.Implementation;
using PrintDesire.BusinessLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PrintDesire.API.Controllers
{
    public class ProductDetailsAPIController : ApiController
    {
        ProductDetails objProductDetails = null;

        public ProductDetailsAPIController()
        {
            objProductDetails = new ProductDetails();
        }

        public IEnumerable<ProductDetailsModel> Get()
        {
            var result = objProductDetails.GetProductDetails();

            return result;
        }

        public ProductDetailsModel Get(int productDetailsId)
        {
            var result = objProductDetails.GetProductDetailsById(productDetailsId);

            return result;
        }

        public bool Post([FromBody]ProductDetailsModel objProductDetailsModel)
        {
            var result = objProductDetails.AddProductDetails(objProductDetailsModel);

            return result;
        }

        public bool Put([FromBody]ProductDetailsModel objProductDetailsModel)
        {
            var result = objProductDetails.UpdateProductDetails(objProductDetailsModel);

            return result;
        }

        public bool Delete(int productDetailsId)
        {
            var mobileModelDetails = objProductDetails.GetProductDetailsById(productDetailsId);

            var result = objProductDetails.RemoveProductDetails(mobileModelDetails);

            return result;
        }

        public bool GetProductDetailsByProductCategoryId(int productCategoryId)
        {
            var result = objProductDetails.GetProductDetailsByProductCategoryId(productCategoryId);

            return result;
        }
    }
}
