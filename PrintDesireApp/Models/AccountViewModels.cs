﻿using PrintDesireApp.Enums;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PrintDesireApp.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }        
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Remember this browser?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }

        public bool IsRegister { get; set; }
    }

    //public class RegisterViewModel
    //{
    //    [Required]
    //    [EmailAddress]
    //    [Display(Name = "Email (*)")]
    //    public string Email { get; set; }

    //    [Required]        
    //    [RegularExpression(@"(?=^.{8,20}$)(?=.*\d)(?=.*\W+)(?![.\n])(?=.*[a-z]).*$", ErrorMessage = "The {0} must be alphanumeric having atleast 1 special, minimum 8 and maximum 20 characters")]
    //    [DataType(DataType.Password)]
    //    [Display(Name = "Password (*)")]
    //    public string Password { get; set; }

    //    [DataType(DataType.Password)]
    //    [Display(Name = "Confirm password (*)")]
    //    [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
    //    public string ConfirmPassword { get; set; }

    //    [Required]
    //    [StringLength(50, ErrorMessage = "First Name - Max 50 letters allowed.")]
    //    [DataType(DataType.Text)]
    //    [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "First Name - Please use Letters only.")]
    //    [Display(Name = "First Name (*)")]
    //    public string FirstName { get; set; }

    //    [Required]
    //    [StringLength(50, ErrorMessage = "Last Name - Max 50 letters allowed.")]
    //    [DataType(DataType.Text)]
    //    [Display(Name = "Last Name (*)")]
    //    [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Last Name - Please use Letters only.")]
    //    public string LastName { get; set; }

    //    [Required]
    //    [StringLength(10, ErrorMessage = "Phone Number - Max 10 digits allowed.")]
    //    [DataType(DataType.PhoneNumber)]
    //    [Display(Name = "Mobile Number (*)")]
    //    [RegularExpression(@"^[0-9]+$", ErrorMessage = "Phone Number - Please use Numbers only.")]
    //    public string PhoneNumber { get; set; }

    //    [Required]
    //    [StringLength(200, ErrorMessage = "Address - Max 200 letters allowed.")]
    //    [DataType(DataType.Text)]
    //    [RegularExpression(@"^[a-zA-Z0-9,]+$", ErrorMessage = "Address - Please use Letters only.")]
    //    [Display(Name = "Address (*)")]
    //    public string Address { get; set; }

    //    [Required]
    //    [StringLength(50, ErrorMessage = "Contact Person - Max 50 letters allowed.")]
    //    [DataType(DataType.Text)]
    //    [Display(Name = "Contact Person (*)")]
    //    [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Contact Person - Please use Letters only.")]
    //    public string ContactPerson { get; set; }

    //    [Required]
    //    [StringLength(15, ErrorMessage = "Registration ID - Max 15 characters allowed.")]
    //    [DataType(DataType.Text)]
    //    [Display(Name = "Registration ID (*)")]
    //    [RegularExpression(@"^[a-zA-Z0-9]+$", ErrorMessage = "Registration ID - Please use letters/digits only.")]
    //    public string RegistrationID { get; set; }

    //    [Display(Name = "Gender (*)")]
    //    public Gender Gender { get; set; }

    //    [Display(Name = "UserType (*)")]
    //    public UserType UserType { get; set; }

    //    public LoginViewModel LoginViewModel { get; set; }
    //}

    public class RegisterViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Required]
        [StringLength(10, ErrorMessage = "Phone Number - Max 10 digits allowed.")]
        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Mobile Number (*)")]
        [RegularExpression(@"^[0-9]+$", ErrorMessage = "Phone Number - Please use Numbers only.")]
        public string PhoneNumber { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "First Name - Max 50 letters allowed.")]
        [DataType(DataType.Text)]
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "First Name - Please use Letters only.")]
        [Display(Name = "First Name (*)")]
        public string FirstName { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "Last Name - Max 50 letters allowed.")]
        [DataType(DataType.Text)]
        [Display(Name = "Last Name (*)")]
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Last Name - Please use Letters only.")]
        public string LastName { get; set; }

        [Required]
        [StringLength(200, ErrorMessage = "Address - Max 200 letters allowed.")]
        [DataType(DataType.Text)]
        [RegularExpression(@"^[a-zA-Z0-9,]+$", ErrorMessage = "Address - Please use Letters/Digits/Comma only.")]
        [Display(Name = "Address (*)")]
        public string Address { get; set; }

        [Display(Name = "Gender (*)")]
        public Gender Gender { get; set; }

        [Display(Name = "UserType (*)")]
        public UserType UserType { get; set; }

        public bool IsTermsAndConditions { get; set; }

        public LoginViewModel LoginViewModel { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}
