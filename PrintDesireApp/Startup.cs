﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PrintDesireApp.Startup))]
namespace PrintDesireApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
