﻿using PrintDesire.Repository.EntityDiagram;
using PrintDesire.Repository.Interface;
using System.Collections.Generic;

namespace PrintDesire.Repository.Implementation
{
    public class ProductCategoryRepository : IProductCategoryRepository
    {
        private readonly IProductCategoryWrapper _objProductCategoryWrapper;

        /// <summary>
        /// Initializes Mobile Company wrapper
        /// </summary>
        public ProductCategoryRepository()
        {
            _objProductCategoryWrapper = new ProductCategoryWrapper();
        }

        /// <summary>
        /// Initializes Mobile Company wrapper with parameter
        /// </summary>
        /// <param name="objMobileCompanyWrapper"></param>
        public ProductCategoryRepository(IProductCategoryWrapper objProductCategoryWrapper)
        {
            _objProductCategoryWrapper = objProductCategoryWrapper;
        }

        /// <summary>
        /// Gets all Mobile Company
        /// </summary>
        /// <returns>Mobile Company collection</returns>
        public IEnumerable<productcategory> GetProductCategory()
        {
            var result = _objProductCategoryWrapper.GetAll();

            return result;
        }

        /// <summary>
        /// Gets Mobile Company By ID
        /// </summary>
        /// <param name="mobileCompanyId"></param>
        /// <returns>Mobile Company</returns>
        public productcategory GetProductCategoryById(int productCategoryId)
        {
            var result = _objProductCategoryWrapper.GetSingle(x => x.ProductCategoryId == productCategoryId);

            return result;
        }

        /// <summary>
        /// Inserts Mobile Company
        /// </summary>
        /// <param name="objMobileCompany"></param>
        /// <returns>Success result true/false</returns>
        public bool AddProductCategory(productcategory objProductCategory)
        {
            bool isSuccess = false;

            _objProductCategoryWrapper.Add(objProductCategory);

            isSuccess = true;

            return isSuccess;
        }

        /// <summary>
        /// Updates Mobile Company
        /// </summary>
        /// <param name="objMobileCompany"></param>
        /// <returns>Success result true/false</returns>
        public bool UpdateProductCategory(productcategory objProductCategory)
        {
            bool isSuccess = false;

            _objProductCategoryWrapper.Update(objProductCategory);

            isSuccess = true;

            return isSuccess;
        }

        /// <summary>
        /// Deletes Mobile Company
        /// </summary>
        /// <param name="objMobileCompany"></param>
        /// <returns>Success result true/false</returns>
        public bool RemoveProductCategory(productcategory objProductCategory)
        {
            bool isSuccess = false;

            _objProductCategoryWrapper.Remove(objProductCategory);

            isSuccess = true;

            return isSuccess;
        }       

    }
}
