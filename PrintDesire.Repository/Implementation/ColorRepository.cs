﻿using PrintDesire.Repository.EntityDiagram;
using PrintDesire.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintDesire.Repository.Implementation
{
   public class ColorRepository:IColorRepository
    {
        private readonly IColorWrapper _objColorWrapper;

        /// <summary>
        /// Initializes Color wrapper
        /// </summary>
        public ColorRepository()
        {
            _objColorWrapper = new ColorWrapper();
        }

        /// <summary>
        /// Initializes Color wrapper with parameter
        /// </summary>
        /// <param name="objMobileCompanyWrapper"></param>
        public ColorRepository(IColorWrapper objColorWrapper)
        {
            _objColorWrapper = objColorWrapper;
        }

        /// <summary>
        /// Gets all Mobile Company
        /// </summary>
        /// <returns>Mobile Company collection</returns>
        public IEnumerable<colors> GetColors()
        {
            var result = _objColorWrapper.GetAll();

            return result;
        }

        /// <summary>
        /// Gets Color By ID
        /// </summary>
        /// <param name="colorId"></param>
        /// <returns>Color</returns>
        public colors GetColorById(int colorId)
        {
            var result = _objColorWrapper.GetSingle(x => x.ColorId == colorId);

            return result;
        }

        /// <summary>
        /// Inserts Color
        /// </summary>
        /// <param name="objColors"></param>
        /// <returns>Success result true/false</returns>
        public bool AddColor(colors objColors)
        {
            bool isSuccess = false;

            _objColorWrapper.Add(objColors);

            isSuccess = true;

            return isSuccess;
        }

        /// <summary>
        /// Updates Color
        /// </summary>
        /// <param name="objColors"></param>
        /// <returns>Success result true/false</returns>
        public bool UpdateColor(colors objColors)
        {
            bool isSuccess = false;

            _objColorWrapper.Update(objColors);

            isSuccess = true;

            return isSuccess;
        }

        /// <summary>
        /// Deletes Color
        /// </summary>
        /// <param name="objColors"></param>
        /// <returns>Success result true/false</returns>
        public bool RemoveColor(colors objColors)
        {
            bool isSuccess = false;

            _objColorWrapper.Remove(objColors);

            isSuccess = true;

            return isSuccess;
        }       
    }
}
